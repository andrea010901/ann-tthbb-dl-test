import glob
import os
import json
import sys
import csv
import math as m
import numpy as np
import ROOT as r

def plotNominalvsVaried(nominal,upVar,downVar,node):

    paveCMS = r.TPaveText(0.12,0.93,0.92,0.96,"NDC");
    paveCMS.AddText("#bf{CMS Simulation 2018} (#it{reconstruction level)}  (13 TeV)")
    paveCMS.SetFillColor(0)
    paveCMS.SetBorderSize(0)
    paveCMS.SetTextSize(0.04)
    paveCMS.SetTextFont(42)

    l0 = r.TLegend(.4, .15, .7, .25)
    l0.AddEntry(nominal,"With MEM","l")
    l0.AddEntry(upVar,"With mbb","l")
    l0.AddEntry(downVar,"Without mem and mbb","l")
    l0.SetTextSize(0.025)

    c = r.TCanvas("c", "canvas", 800, 800)
    maximum = max(upVar.GetMaximum(),downVar.GetMaximum(),nominal.GetMaximum())
    maximum = maximum+0.05*maximum
    upVar.SetLineColor(r.kRed)
    upVar.SetMarkerColor(r.kRed)
    upVar.SetLineWidth(2)
    upVar.SetFillColorAlpha(r.kRed, 0.35);
    downVar.SetLineColor(r.kBlue)
    downVar.SetMarkerColor(r.kBlue)
    downVar.SetLineWidth(2)
    downVar.SetFillColorAlpha(r.kBlue, 0.35);
    nominal.SetLineColor(r.kBlack)
    nominal.SetMarkerColor(r.kBlack)
    nominal.SetLineWidth(2)
    nominal.SetFillColorAlpha(r.kBlack, 0.35);
    nominal.SetMaximum(maximum)
    nominal.Draw("HIST")
    upVar.Draw("HIST same") 
    downVar.Draw("HIST same")
    paveCMS.Draw("same")
    l0.Draw("same")
    c.SaveAs(node+'_node.png')

r.gStyle.SetOptStat(0)
r.gROOT.SetBatch(True)

ttHbb_mem = r.TH1F("ttHbb-MEM",";ttHbb Node;Number of events/Bin width ",20,0,1)
ttHbb_mbb = r.TH1F("ttHbb-mbb",";ttHbb Node;Number of events/Bin width ",20,0,1) 
ttHbb_none = r.TH1F("ttHbb-none",";ttHbb Node;Number of events/Bin width ",20,0,1)

ttbb_mem = r.TH1F("ttbb-MEM",";ttbb Node;Number of events/Bin width ",20,0,1)
ttbb_mbb = r.TH1F("ttbb-mbb",";ttbb Node;Number of events/Bin width ",20,0,1)
ttbb_none = r.TH1F("ttbb-none",";ttbb Node;Number of events/Bin width ",20,0,1)

ttcc_mem = r.TH1F("ttcc-MEM",";ttcc Node;Number of events/Bin width ",20,0,1)
ttcc_mbb = r.TH1F("ttcc-mbb",";ttcc Node;Number of events/Bin width ",20,0,1)
ttcc_none = r.TH1F("ttcc-none",";ttcc Node;Number of events/Bin width ",20,0,1)

ttlf_mem = r.TH1F("ttlf-MEM",";ttlf Node;Number of events/Bin width ",20,0,1)
ttlf_mbb = r.TH1F("ttlf-mbb",";ttlf Node;Number of events/Bin width ",20,0,1)
ttlf_none = r.TH1F("ttlf-none",";ttlf Node;Number of events/Bin width ",20,0,1)


files = ['ttH_out_MEM.root','ttH_out_mbb.root','ttH_out_none.root','tt_out_MEM.root','tt_out_mbb.root','tt_out_none.root']

f = r.TFile.Open(files[0])
for event in f.Get("liteTreeTTH_step7_cate9"):
    if(event.N_btags_Medium>2 and event.MEM>=0 and event.mbb>=0): 
        DNN_out = [event.ttHbb,event.ttbb,event.ttcc,event.ttlf]
        maxOut = max(DNN_out)
        max_index = DNN_out.index(maxOut)
        if(max_index==0):
            ttHbb_mem.Fill(event.ttHbb)
        if(max_index==1):
            ttbb_mem.Fill(event.ttbb)
        if(max_index==1):
            ttcc_mem.Fill(event.ttcc)
        if(max_index==1):
            ttlf_mem.Fill(event.ttlf)
        del DNN_out[:]
f.Close()

f = r.TFile.Open(files[1])
for event in f.Get("liteTreeTTH_step7_cate9"):
    if(event.N_btags_Medium>2 and event.MEM>=0 and event.mbb>=0):
        DNN_out = [event.ttHbb,event.ttbb,event.ttcc,event.ttlf]
        maxOut = max(DNN_out)
        max_index = DNN_out.index(maxOut)
        if(max_index==0):
            ttHbb_mbb.Fill(event.ttHbb)
        if(max_index==1):
            ttbb_mbb.Fill(event.ttbb)
    	if(max_index==1):
            ttcc_mbb.Fill(event.ttcc)
    	if(max_index==1):
            ttlf_mbb.Fill(event.ttlf)
        del DNN_out[:]
f.Close()

f = r.TFile.Open(files[2])
for event in f.Get("liteTreeTTH_step7_cate9"):
    if(event.N_btags_Medium>2 and event.MEM>=0 and event.mbb>=0):
        DNN_out = [event.ttHbb,event.ttbb,event.ttcc,event.ttlf]
    	maxOut = max(DNN_out)
    	max_index = DNN_out.index(maxOut)
    	if(max_index==0):
            ttHbb_none.Fill(event.ttHbb)
        if(max_index==1):
            ttbb_none.Fill(event.ttbb)
        if(max_index==1):
            ttcc_none.Fill(event.ttcc)
        if(max_index==1):
            ttlf_none.Fill(event.ttlf)  
        del DNN_out[:]   
f.Close()

plotNominalvsVaried(ttHbb_mem,ttHbb_mbb,ttHbb_none,'ttHbb')
plotNominalvsVaried(ttbb_mem,ttbb_mbb,ttbb_none,'ttbb')
plotNominalvsVaried(ttcc_mem,ttcc_mbb,ttcc_none,'ttcc')
plotNominalvsVaried(ttlf_mem,ttlf_mbb,ttlf_none,'ttlf')

