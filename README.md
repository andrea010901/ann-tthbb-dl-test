This framework is used to train and evaluate a ANN model using tensorflow and keras.


## Instructions

**For preparing the environment :**

1. Run under a CMSSW release
2. Export the Keras back-end
3. Set PYTHONHOME path

**Commands to run :**

**Step-up CMSSW**
```
cmsrel CMSSW_13_0_0
cd CMSSW_13_0_0/src
cmsenv
cd ../..
export KERAS_BACKEND=tensorflow
```

Clone the repository:
```
git clone https://gitlab.cern.ch/ckoraka/ann-tthbb-dl.git
cd ann-tthbb-dl
```
**To run the training script :**
```
python3 train_model_binary.py
```

Two files are produced when running the model training. The **model.h5** file which contains the model weights and architecture and the **variable_norm.csv** file which contains the input feature 
normalization values needed for the evaluation script.


**To run the evaluation script (updated for python3) :**
```
python3  evaluate.py input.root output.root conf.json
```
where :

1. **input** Input .root file
2. **output** Output .root file
3. **conf.json** Json file with configs


**To run the evaluation script :** 
```
python  evaluate_model.py -i input -o output -j config.json
```

where :


1. **input** Input .root file 
2. **output** Output .root file
3. **config.json** Json file with configs 

The evaluation script produces a root file with the values of the output nodes and any additional branch from the input tree - should configue the **config.json** file accordingly.
